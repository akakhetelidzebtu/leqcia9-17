package com.btu.myapplication_9_17

import android.content.Intent
import android.net.Uri
import android.net.Uri.parse
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URI

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        d("activitylifecycle","onCreateActivity1")
        button.setOnClickListener {
            val intent = Intent(this,MainActivity2::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        d("activitylifecycle","onStartActivity1")
    }

    override fun onResume() {
        super.onResume()
        d("activitylifecycle","onResumeActivity1")
    }

    override fun onPause() {
        super.onPause()
        d("activitylifecycle","onPauseActivity1")
    }

    override fun onStop() {
        super.onStop()
        d("activitylifecycle","onStopActivity1")
    }

    override fun onDestroy() {
        super.onDestroy()
        d("activitylifecycle","onDestroyActivity1")
    }

}